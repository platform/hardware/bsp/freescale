#!/bin/bash

# partition size in MB
BOOTLOAD_RESERVE=8
BOOT_ROM_SIZE=16
SYSTEM_ROM_SIZE=512
CACHE_SIZE=512
RECOVERY_ROM_SIZE=16
BOOT_ROM_B_SIZE=16
SYSTEM_ROM_B_SIZE=512
MISC_SIZE=8

help() {

bn=`basename $0`
cat << EOF
usage $bn <option> device_node

options:
  -h				displays this help message
  -s				only get partition size
  -np 				not partition.
  -f   				flash android image.
EOF

}

# parse command line
moreoptions=1
node="na"
soc_name=""
cal_only=0
flash_images=0
not_partition=0
not_format_fs=0
bootloader_file="u-boot.imx"
bootimage_file="boot.img"
systemimage_file="system.img"
recoveryimage_file="recovery.img"
miscimg_file="misc.img"

while [ "$moreoptions" = 1 -a $# -gt 0 ]; do
	case $1 in
	    -h) help; exit ;;
	    -s) cal_only=1 ;;
	    -f) flash_images=1 ;;
	    -np) not_partition=1 ;;
	    -nf) not_format_fs=1 ;;
	    *)  moreoptions=0; node=$1 ;;
	esac
	[ "$moreoptions" = 0 ] && [ $# -gt 1 ] && help && exit
	[ "$moreoptions" = 1 ] && shift
done

if [ "${node}" = "/dev/sda" ]; then
        echo "====== dangous!"
        exit
fi

if [ ! -e ${node} ]; then
	echo "no such node ${node}"
	help
	exit
fi
# call sfdisk to create partition table
# get total card size
seprate=40
total_size=`sfdisk -s ${node}`
total_size=`expr ${total_size} / 1024`
boot_rom_sizeb=`expr ${BOOT_ROM_SIZE} + ${BOOTLOAD_RESERVE}`
extend_size=`expr ${SYSTEM_ROM_SIZE} + ${CACHE_SIZE} + ${BOOT_ROM_B_SIZE} + ${SYSTEM_ROM_B_SIZE} + ${MISC_SIZE} + ${seprate}`
data_size=`expr ${total_size} - ${boot_rom_sizeb} - ${RECOVERY_ROM_SIZE} - ${extend_size} + ${seprate}`

echo "total_size $total_size"
echo "boot_rom_sizeb $boot_rom_sizeb"
echo "extend_size $extend_size"
echo "data_size $data_size"

# create partitions
if [ "${cal_only}" -eq "1" ]; then
cat << EOF
BOOT   : ${boot_rom_sizeb}MB
RECOVERY: ${RECOVERY_ROM_SIZE}MB
SYSTEM : ${SYSTEM_ROM_SIZE}MB
CACHE  : ${CACHE_SIZE}MB
DATA   : ${data_size}MB
BOOT_B : ${BOOT_ROM_B_SIZE}MB
SYSTEM_B : ${SYSTEM_ROM_B_SIZE}MB
MISC : ${MISC_SIZE}MB
EOF
exit
fi

function format_android
{
    echo "formating android images"
    mkfs.ext4 ${node}${part}4 -Ldata
    mkfs.ext4 ${node}${part}5 -Lsystem_a
    mkfs.ext4 ${node}${part}6 -Lcache
    mkfs.ext4 ${node}${part}8 -Lsystem_b
}

#SIMG2IMGTOOL=~/maddev_brillo/out/host/linux-x86/bin/simg2img
function flash_android
{
#   bootloader_file="u-boot-${soc_name}.imx"
#   bootimage_file="boot-${soc_name}.img"
#   recoveryimage_file="recovery-${soc_name}.img"

if [ "${flash_images}" -eq "1" ]; then
    echo "flashing android images..."
    echo "bootloader: ${bootloader_file}"
    echo "boot image: ${bootimage_file}"
    echo "recovery image: ${recoveryimage_file}"
    echo "system image: ${systemimage_file}"
    echo "misc image: ${miscimg_file}"

    dd if=/dev/zero of=${node} bs=1k seek=384 count=129
    dd if=${bootloader_file} of=${node} bs=1k seek=1
    dd if=${bootimage_file} of=${node}${part}1
    dd if=${recoveryimage_file} of=${node}${part}2
    simg2img ${systemimage_file} ${node}${part}5
    dd if=${bootimage_file} of=${node}${part}7
    simg2img ${systemimage_file} ${node}${part}8
    dd if=${miscimg_file} of=${node}${part}9
    sync
fi
}

if [[ "${not_partition}" -eq "1" && "${flash_images}" -eq "1" ]] ; then
    flash_android
    exit
fi


sfdisk --force -uM ${node} << EOF
,${boot_rom_sizeb},83
,${RECOVERY_ROM_SIZE},83
,${extend_size},5
,${data_size},83
,${SYSTEM_ROM_SIZE},83
,${CACHE_SIZE},83
,${BOOT_ROM_B_SIZE},83
,${SYSTEM_ROM_B_SIZE},83
,${MISC_SIZE},83
EOF

# adjust the partition reserve for bootloader.
# if you don't put the uboot on same device, you can remove the BOOTLOADER_ERSERVE
# to have 8M space.
# the minimal sylinder for some card is 4M, maybe some was 8M
# just 8M for some big eMMC 's sylinder
sfdisk --force -uM ${node} -N1 << EOF
${BOOTLOAD_RESERVE},${BOOT_ROM_SIZE},83
EOF

# format the SDCARD/DATA/CACHE partition
part=""
echo ${node} | grep mmcblk > /dev/null
if [ "$?" -eq "0" ]; then
	part="p"
fi

format_android
flash_android


# For MFGTool Notes:
# MFGTool use mksdcard-android.tar store this script
# if you want change it.
# do following:
#   tar xf mksdcard-android.sh.tar
#   vi mksdcard-android.sh
#   [ edit want you want to change ]
#   rm mksdcard-android.sh.tar; tar cf mksdcard-android.sh.tar mksdcard-android.sh
